module gitlab.com/simon.schlumpf/dbconfig

go 1.16

require (
	github.com/go-chi/chi/v5 v5.1.0
	github.com/jackc/pgx/v4 v4.11.0
)
