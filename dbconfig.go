package dbconfig

import (
	"context"
	"database/sql"
	"fmt"
	"log"
	"runtime"
	"strings"

	"github.com/jackc/pgx/v4"
)

// DBConfig holds the DB connection
type DBConfig struct {
	dbconn *pgx.Conn
}

// ConfigItem represents a configuration item in the database
type ConfigItem struct {
	Name        string
	StringValue sql.NullString
	IntValue    sql.NullInt64
}

// Logger is an interface that your custom logger needs to implement
type Logger interface {
	Printf(format string, v ...interface{})
	Println(v ...interface{})
}

// defaultLogger is the package-level logger. It defaults to the standard log package.
var packageLogger Logger = log.Default()

// SetLogger allows the main program to set a custom logger.
func SetLogger(customLogger Logger) {
	packageLogger = customLogger
}

// Initialise initializes the dbconfig system with DB connection etc.
func (dbc *DBConfig) Initialise(dbuser string, dbpass string, dbhost string, dbname string) (err error) {
	dbc.dbconn, err = pgx.Connect(context.Background(), "postgres://"+dbuser+":"+dbpass+"@"+dbhost+"/"+dbname)
	return
}

// GetConfString retrieves string config from the DB
func (dbc *DBConfig) GetConfString(name string) (res string, err error) {
	rows, err := dbc.dbconn.Query(context.Background(), `SELECT "stringValue" FROM public."Config" WHERE "name" = $1 LIMIT 1`, name)
	if err != nil {
		return
	}
	for rows.Next() {
		if err = rows.Scan(&res); err != nil {
			return
		}
	}
	rows.Close()
	res = strings.TrimSpace(res)
	return
}

func (dbc *DBConfig) logError(err error) {
	if err != nil {
		_, file, line, ok := runtime.Caller(1)
		if !ok {
			line = 0
			file = "unknown file"
		}
		packageLogger.Printf("dbconf: errorin %s on line %d: %s", file, line, err.Error())
	}
}
func (dbc *DBConfig) logInfo(msg string) {
	packageLogger.Printf("dbconf: %s", msg)
}

// GetConfStringWithDefault retrieves string config from the DB or returns the default value if not found or empty
func (dbc *DBConfig) GetConfStringWithDefault(name string, defaultValue string) (res string, err error) {
	// Call the existing GetConfString function
	res, err = dbc.GetConfString(name)
	if err != nil {
		dbc.logError(err)
		return defaultValue, err
	}

	// If the result is empty, return the default value
	if strings.TrimSpace(res) == "" {
		return defaultValue, nil
	}

	return res, nil
}

// SetConfString writes (creates or Updates) a config parameter
func (dbc *DBConfig) SetConfString(name string, value string) (err error) {
	dbc.logInfo(fmt.Sprintf("setting string config item \"%s\" to \"%s\"", name, value))
	_, err = dbc.dbconn.Exec(context.Background(), `INSERT INTO public."Config"("name", "stringValue") VALUES($1,$2) ON CONFLICT (name) DO UPDATE set "stringValue"=$2`, name, value)
	dbc.logError(err)
	return err
}

// GetConfInt retrieves integer config from the DB
func (dbc *DBConfig) GetConfInt(name string) (res int64, err error) {
	rows, err := dbc.dbconn.Query(context.Background(), `SELECT "intValue" FROM public."Config" WHERE "name" = $1 LIMIT 1`, name)
	if err != nil {
		dbc.logError(err)
		return
	}
	for rows.Next() {
		if err = rows.Scan(&res); err != nil {
			dbc.logError(err)
			return
		}
	}
	rows.Close()
	return
}

// DeleteConf deletes a configuration item from the database
func (dbc *DBConfig) DeleteConf(name string) error {
	dbc.logInfo(fmt.Sprintf("deleting config item \"%s\"", name))
	_, err := dbc.dbconn.Exec(context.Background(), `DELETE FROM public."Config" WHERE "name" = $1`, name)
	if err != nil {
		dbc.logError(err)
		return err
	}
	return nil
}

// GetConfIntWithDefault retrieves integer config from the DB or returns the default value if not found
func (dbc *DBConfig) GetConfIntWithDefault(name string, defaultValue int64) (res int64, err error) {
	// Call the existing GetConfInt function
	res, err = dbc.GetConfInt(name)
	if err != nil {
		dbc.logError(err)
		return defaultValue, err
	}

	// If the result is zero (or another condition indicating absence), return the default value
	// You can adjust this condition based on your specific needs
	if res == 0 && err == nil {
		return defaultValue, nil
	}

	return res, nil
}

// SetConfInt writes (creates or Updates) a config parameter with an integer value
func (dbc *DBConfig) SetConfInt(name string, value int64) (err error) {
	dbc.logInfo(fmt.Sprintf("setting int config item \"%s\" to \"%d\"", name, value))
	_, err = dbc.dbconn.Exec(context.Background(), `INSERT INTO public."Config"("name", "intValue") VALUES($1,$2) ON CONFLICT (name) DO UPDATE SET "intValue"=$3`, name, value)
	dbc.logError(err)
	return err
}

// GetConfigItem retrieves a single configuration item from the database
func (dbc *DBConfig) GetConfigItem(name string) (*ConfigItem, error) {
	var item ConfigItem
	err := dbc.dbconn.QueryRow(context.Background(),
		`SELECT "name", "stringValue", "intValue" FROM public."Config" WHERE "name" = $1`, name).
		Scan(&item.Name, &item.StringValue, &item.IntValue)
	if err != nil {
		return nil, err
	}
	return &item, nil
}

func (dbc *DBConfig) LoadAllConfig() ([]ConfigItem, error) {
	rows, err := dbc.dbconn.Query(context.Background(), `SELECT "name", "stringValue", "intValue" FROM public."Config"`)
	if err != nil {
		dbc.logError(err)
		return nil, err
	}
	defer rows.Close()

	var configs []ConfigItem
	for rows.Next() {
		var item ConfigItem
		if err = rows.Scan(&item.Name, &item.StringValue, &item.IntValue); err != nil {
			dbc.logError(err)
			return nil, err
		}
		configs = append(configs, item)
	}
	return configs, nil
}
