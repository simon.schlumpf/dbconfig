package main

import (
	"log"

	"gitlab.com/simon.schlumpf/dbconfig"
)

func main() {
	// Initialize the DBConfig instance
	var dbc dbconfig.DBConfig

	// Initialize the database connection (replace with actual credentials)
	err := dbc.Initialise("ipdb", "ipdb", "localhost:54320", "ipdb")
	if err != nil {
		log.Fatalf("Could not initialize DB: %v", err)
	}

	// Optionally, set a custom logger (if needed)
	// Example using the standard log package
	dbconfig.SetLogger(log.Default())

	// Start the web server for testing purposes
	if err := dbc.StartWebServer("localhost", 5555); err != nil {
		log.Fatalf("Could not start server: %v", err)
	}
}
