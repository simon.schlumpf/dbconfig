package dbconfig

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"html/template"
	"net/http"
	"strings"

	"github.com/go-chi/chi/v5"
	"github.com/go-chi/chi/v5/middleware"
)

// StartWebServer starts the web server for editing the configuration
func (dbc *DBConfig) StartWebServer(host string, port int) error {
	r := chi.NewRouter()
	r.Use(middleware.Logger)

	r.Get("/", dbc.handleConfigView)                    // Get all configurations
	r.Post("/configs/", dbc.handleConfigCreate)         // Create a new configuration
	r.Get("/configs/{name}", dbc.handleConfigGet)       // Get a specific configuration
	r.Put("/configs/{name}", dbc.handleConfigUpdate)    // Update a specific configuration
	r.Delete("/configs/{name}", dbc.handleConfigDelete) // Delete a specific configuration

	startparam := fmt.Sprintf("%s:%d", host, port)
	packageLogger.Printf("Starting server on %s...", startparam)
	return http.ListenAndServe(startparam, r)
}

// handleConfigDelete handles deleting a configuration item
func (dbc *DBConfig) handleConfigDelete(w http.ResponseWriter, r *http.Request) {
	name := chi.URLParam(r, "name")

	// Call the DeleteConf method to handle the deletion
	err := dbc.DeleteConf(name)
	if err != nil {
		dbc.logError(err)
		http.Error(w, "Could not delete config", http.StatusInternalServerError)
		return
	}

	// Re-render the section after deletion
	dbc.handleConfigView(w, r)
}

// handleConfigView renders the configuration view page
func (dbc *DBConfig) handleConfigView(w http.ResponseWriter, r *http.Request) {
	configs, err := dbc.LoadAllConfig()

	if err != nil {
		dbc.logError(err)
		http.Error(w, fmt.Sprintf("Could not load config: %s", err.Error()), http.StatusInternalServerError)
		return
	}

	groupedConfigs := groupConfigs(configs)

	tmpl, err := template.ParseFiles("dbconfig_web.html") // Load the template from the file
	if err != nil {
		dbc.logError(err)
		http.Error(w, "Could not parse template", http.StatusInternalServerError)
		return
	}
	if err := tmpl.Execute(w, groupedConfigs); err != nil {
		dbc.logError(err)
		http.Error(w, "Could not render template", http.StatusInternalServerError)
	}
}

// handleConfigUpdate handles updating an existing configuration item
func (dbc *DBConfig) handleConfigUpdate(w http.ResponseWriter, r *http.Request) {
	name := chi.URLParam(r, "name") // Get the name from the URL parameter
	stringValue := r.FormValue("stringValue")
	intValue := r.FormValue("intValue")

	var sqlStringValue sql.NullString
	var sqlIntValue sql.NullInt64

	if stringValue != "" {
		sqlStringValue = sql.NullString{String: stringValue, Valid: true}
	} else {
		sqlStringValue = sql.NullString{Valid: false}
	}

	if intValue != "" {
		var iv int64
		_, err := fmt.Sscanf(intValue, "%d", &iv)
		if err != nil {
			http.Error(w, "Invalid integer value", http.StatusBadRequest)
			return
		}
		sqlIntValue = sql.NullInt64{Int64: iv, Valid: true}
	} else {
		sqlIntValue = sql.NullInt64{Valid: false}
	}

	// Assuming you have an UpdateConfInt and UpdateConfString function to update existing configs
	if sqlIntValue.Valid {
		err := dbc.SetConfInt(name, sqlIntValue.Int64)
		if err != nil {
			http.Error(w, "Could not update config", http.StatusInternalServerError)
			return
		}
	} else {
		err := dbc.SetConfString(name, sqlStringValue.String)
		if err != nil {
			http.Error(w, "Could not update config", http.StatusInternalServerError)
			return
		}
	}

	// Redirect to the config view page or send a success response
	http.Redirect(w, r, "/configs", http.StatusSeeOther)
}

// handleConfigCreate handles creating a new configuration item
func (dbc *DBConfig) handleConfigCreate(w http.ResponseWriter, r *http.Request) {
	name := r.FormValue("name")
	stringValue := r.FormValue("stringValue")
	intValue := r.FormValue("intValue")

	var sqlStringValue sql.NullString
	var sqlIntValue sql.NullInt64

	if stringValue != "" {
		sqlStringValue = sql.NullString{String: stringValue, Valid: true}
	} else {
		sqlStringValue = sql.NullString{Valid: false}
	}

	if intValue != "" {
		var iv int64
		_, err := fmt.Sscanf(intValue, "%d", &iv)
		if err != nil {
			http.Error(w, "Invalid integer value", http.StatusBadRequest)
			return
		}
		sqlIntValue = sql.NullInt64{Int64: iv, Valid: true}
	} else {
		sqlIntValue = sql.NullInt64{Valid: false}
	}

	// Use the existing SetConfInt and SetConfString functions to create new configs
	if sqlIntValue.Valid {
		dbc.SetConfInt(name, sqlIntValue.Int64)
	} else {
		dbc.SetConfString(name, sqlStringValue.String)
	}

	// Redirect to the config view page or send a success response
	http.Redirect(w, r, "/configs", http.StatusSeeOther)
}

// groupConfigs groups config items by their prefix
func groupConfigs(configs []ConfigItem) map[string][]ConfigItem {
	grouped := make(map[string][]ConfigItem)
	for _, item := range configs {
		parts := strings.SplitN(item.Name, ".", 2)
		if len(parts) > 1 {
			grouped[parts[0]] = append(grouped[parts[0]], ConfigItem{Name: parts[1], StringValue: item.StringValue, IntValue: item.IntValue})
		} else {
			grouped[parts[0]] = append(grouped[parts[0]], item)
		}
	}
	return grouped
}

// handleConfigGet retrieves a specific configuration item
func (dbc *DBConfig) handleConfigGet(w http.ResponseWriter, r *http.Request) {
	name := chi.URLParam(r, "name") // Extract the 'name' from the URL

	// Fetch the configuration item from the database
	configItem, err := dbc.GetConfigItem(name)
	if err != nil {
		if err == sql.ErrNoRows { // Handle the case where the config item doesn't exist
			http.Error(w, "Configuration not found", http.StatusNotFound)
			return
		}
		http.Error(w, "Error fetching configuration", http.StatusInternalServerError)
		return
	}

	// Send the configuration item as JSON
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(configItem)
}
